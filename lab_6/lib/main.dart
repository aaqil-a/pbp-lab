import 'package:flutter/material.dart';

import 'screens/home_screen.dart';

void main() => runApp(StotesApp());

class StotesApp extends StatefulWidget {
  @override
  _StotesAppState createState() => _StotesAppState();
}

class _StotesAppState extends State<StotesApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stotes',
      theme: ThemeData(
        primaryColor: Color.fromRGBO(195, 197, 173, 1),
        accentColor: Color.fromRGBO(52, 30, 11, 1),
        canvasColor: Color.fromRGBO(255, 255, 255, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(52, 30, 11, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => MeetingsScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
    );
  }
}

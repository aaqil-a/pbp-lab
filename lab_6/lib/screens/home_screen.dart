import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class MeetingsScreen extends StatefulWidget {
  @override
  _MeetingsScreenState createState() => _MeetingsScreenState();
}

class _MeetingsScreenState extends State<MeetingsScreen> {
  Widget buildListTile(String title, IconData icon) {
    return ListTile(
      leading: Icon(
        icon,
        size: 20,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  final ButtonStyle style = ElevatedButton.styleFrom(
    primary: Color.fromRGBO(102, 108, 78, 1),
    minimumSize: Size(0, 56),
  );
  final ButtonStyle deleteStyle = ElevatedButton.styleFrom(
    primary: Color.fromRGBO(163, 82, 73, 1),
    minimumSize: Size(0, 56),
  );
  final TextStyle buttonTextStyle = TextStyle(
    fontWeight: FontWeight.w300,
    fontSize: 16,
    color: Colors.white,
  );

  Widget buildCard(String title, String time) {
    return Card(
      color: Color.fromRGBO(253, 248, 238, 1),
      child: Column(children: [
        buildListTile(title, Icons.person),
        buildListTile(time, Icons.calendar_today),
        Divider(),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: ElevatedButton(
                      style: style,
                      onPressed: () {},
                      child: Text('Link', style: buttonTextStyle))),
              Expanded(
                  child: ElevatedButton(
                      style: style,
                      onPressed: () {},
                      child: Text('Edit', style: buttonTextStyle))),
              Expanded(
                  child: ElevatedButton(
                      style: deleteStyle,
                      onPressed: () {},
                      child: Text('Delete', style: buttonTextStyle))),
            ],
          ),
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meetings',
            style: TextStyle(
              fontWeight: FontWeight.w200,
              fontSize: 24,
              color: Color.fromRGBO(95, 69, 31, 1),
            )),
      ),
      drawer: MainDrawer(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 48),
            child: Text(
              'Your Upcoming Meetings',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 40,
                  color: Theme.of(context).accentColor),
            ),
          ),
          Expanded(
              child: ListView(children: [
            buildCard('Meeting 1', '12 November 13:00'),
            buildCard('Meeting 2', '14 November 21:00'),
            buildCard('Meeting 3', '15 November 15:00'),
            buildCard('Meeting 4', '17 November 16:00'),
          ])),
        ]),
      ),
    );
  }
}

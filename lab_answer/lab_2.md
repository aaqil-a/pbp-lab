# Lab 2 Answers
### Apakah perbedaan antara JSON dan XML?
The difference between JSON and XML is actually in their names. XML is a markup language, meaning that it consists of plain text formatted by tags. Meanwhile, JSON is an object notation, meaning that it represents an object in the typical hierarchial format, similair to the one used in JavaScript.

This means that XML is better suited for storing documents mainly containing text, and JSON is better suited for storing typical programming objects and data structures.
### Apakah perbedaan antara HTML dan XML?
While they are both markup languages, HTML and XML are different in the way that they are used. HTML is used for creating and displaying a web page's structure and contents. Meanwhile, XML is designed and used only for carrying data, not displaying it.
